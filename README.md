# About
This is a simple application for having a replicated log. You can store simple json blobs in memory replicated over how many nodes you want without single point of failure. 

Please note that this is more of a proof of concept than a working system.

# Setup
- `git clone git@bitbucket.org:victor_axelsson/replicatedlog.git`
- `npm install`

Optional step: 
- `npm install nodemon -g`

You can check the existing configuration files in the `node/configurations` folder. You can either tweek or add new files as you see fit. 

# Firing up a cluster 

Next you need to fire up some nodes. I prefer doing this with nodemon since the nodes will get restarted when you save a file. The problem with this is that all servers will "crash" and loose it's memory (amnesia) since everything is stored in memory. Therefore, I like to keep one server started with `node` so it doens't restart and can provide the other nodes with the log and the topology (don't worry too much, you will see what I mean when you test it out). 

Example of starting a cluster of 5: 

- `node node node1` <- this will be the "permanent" node
- `nodemon node node2` 
- `nodemon node node3` 
- `nodemon node node4` 
- `nodemon node node5` 

The third parameter is corresponding to the configuration file. So it you have named them differently you need to adust for that. 


# Creating an inital topology

It's pretty simple. You just use the CLI option 1 on each node. You can print the options with the numer 9. You should get something like: 

```
--------------------- <Node> ---------------------
use <CTRL> + <C> to exit the application

1, Send ping to replica
2, Print current topology
3, Print suspected
4, Execute query
5, Print store
6, Print clock
9, Print this menu
--------------------- </Node> --------------------
```

Let's assume you have 5 nodes running on localhost on ports 3000-3004. 

On node1:

- `1`
- http://localhost,3000

On node2:

- `1`
- http://localhost,3000

etc...

Note how the port number is delimited by a comma (because  of reasons) and not a colon. You also don't have to ping the same node. You can ping any node in the cluster and the topology will be shared. 

# Running queries
The query language is based on traversals on an object. Currently, arrays are not supported but feel free to make a pull request. 

A query is based on three parts where the last one is optional. 

`(<traversal>)(:<action>)(:values <values>)`

The traversal defines where in the object we are executing the query. The `:<action>` defines if it's a `:get`, `:set` or `:delete`. The `:values` defined what values are to be used with `:set` action. The values can either be a json object or strings that are escaped in quatations. 

Queries are performed with a http POST to any node in the cluser. Example:

```
POST localhost:3000/v1/query 
{
	"query": "(users,0,username)(:set)(:values {\"username\":\"Dudesson\"})"
}
```

Or: 
```
POST localhost:3000/v1/query 
{
	"query": "(users,0,username)(:delete)"
}
```
Or: 
```
POST localhost:3000/v1/query 
{
	"query": "(users,0)(:get)"
}
```
Or: 
```
POST localhost:3000/v1/query 
{
	"query": "(users,0,username)(:set)(:values \"Dudesson\")"
}
```

# Pull requests?
Yes, please. The code needs you...
