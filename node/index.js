var fs = require('fs');
var broadcast = require('./broadcast')
var stdIO = require('./stdIO')
var topologyManager = require('./broadcast/topologyManager')
var failureDetector = require('./failureDetector')
var storage = require('./storage')
var clock = require('./broadcast/clock')

if(process.argv.length < 2){
	console.log("You need to specify a configuration");
	return;
}

var configPath = __dirname + "/configurations/" + process.argv[2]; 
if(!fs.existsSync(configPath + ".js")) throw "No such configuration";

//Grab the config and that the broadcast listener
var config = require(configPath); 
broadcast.startListener(config); 
storage.setConfig(config); 

var preCatch = null; 
stdIO.startInputListener(function(line){

	if(preCatch){
		preCatch(line); 
	}

	if(line == "1"){
		console.log("Ok, so you wanna ping into the topology...")
		console.log("Gimme the ip and port of the other node, sepparated by comma (q to cancel):")
		preCatch = function(_line){
			if(_line == "q"){
				preCatch = null; 
				stdIO.printMenu();
				return;
			}

			//Do some validation I guess, but yeah... whatever just type it right
			var pt = _line.split(",");
			broadcast.sendPing({ip: pt[0], port: pt[1]});
			preCatch = null; 
		}
	}else if(line == "2"){
		console.log(topologyManager.getTopology()); 
	}else if(line == "3"){
		console.log(failureDetector.getSuspected()); 
	}else if(line == "4"){
		preCatch = function(query){
			var transaction = storage.createTransaction(query);
			broadcast.bestEffortBroadcast(transaction, "query", () => {
				console.log("Ok, its delivered")
				storage.commit(transaction); 
			})
			preCatch = null; 
		}
	}else if(line == "5"){
		console.log(storage.getStore()); 
	}else if(line == "6"){
		console.log(clock.getClock()); 
	}else if(line == "9"){
		stdIO.printMenu();
	}
})
stdIO.printMenu();

