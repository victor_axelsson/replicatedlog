var readline = require('readline');

var stdIO = {}

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

stdIO.printMenu = function(){
	var menu = ""; 
	menu += "--------------------- <Node> --------------------- \n"; 
	menu += "use <CTRL> + <C> to exit the application \n\n"
	menu += "1, Send ping to replica \n"
	menu += "2, Print current topology \n"
	menu += "3, Print suspected \n"
	menu += "4, Execute query \n"
	menu += "5, Print store \n"
	menu += "6, Print clock \n"
	menu += "9, Print this menu \n"
	menu += "--------------------- </Node> -------------------- \n"; 
	console.log(menu)
}

stdIO.startInputListener = function(onMessage){
	rl.on('line', function(line){
	    if(!!line && line.trim().length > 0){
	    	var res = onMessage(line)

	    	if(res == null) return; 
	    }
	});
}

module.exports = stdIO;