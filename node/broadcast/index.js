var request = require("request"); 
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json({limit: '50mb'}));
var CronJob = require('cron').CronJob;
var failureDetector = require('../failureDetector')
var storage = require('../storage')
var clock = require('./clock'); 

var config = null; 

var topologyManager = require("./topologyManager"); 

var broadcast = {}; 
var messages = {};


var router = {
	'ping': (req, res) => {
		var message = req.body; 

		//Merge the recieved topology
		topologyManager.mergeTopology(message.topology, config)

		//If this node was suspected, retore it
		if(failureDetector.isSuspected(message.ip, message.port)){
			failureDetector.restore(message.ip, message.port); 
		}

		//We are beind
		if(message.clock > clock.getClock()){
			console.log("Ok, I'm behind I will need to catch up")
			askForLog(message);
		}

		res.status(200).send(topologyManager.getTopology())
	},
	'clientQuery' : (req, res) => {

		clock.tick(); 
		
		var message = req.body; 
		var transaction = storage.createTransaction(message.query);

		//Broadcast the transaction and respond when you have quorum majority
		var msg = broadcast.bestEffortBroadcast(transaction, "query", () => {
			var commitResult = storage.commit(transaction); 

			res.status(200).send(commitResult); 
		});

		message.timestamp = msg.timestamp;
		
		onMessageReceive(message, transaction); 

	},
	'query': (req, res) => {
		var message = req.body; 
		console.log("[" + config.name + "] received message from [" + req.body.sender + "]"); 

		//Update local clock
		clock.merge(message.timestamp, clock.getClock()); 

		var transaction = message.message;
		
		//Do eager broadcasting
		if(!messages[message.timestamp]){

			broadcast.bestEffortBroadcast(transaction, "query", () => {
				var commitResult = storage.commit(transaction); 
				//insert into users (username, password) values (kalle, kula)
			}, message.timestamp); 
		}

		onMessageReceive(message, transaction);
		
		//Ack message
		res.status(200).send({status: 'ok'})
	},
	'askForLog': (req, res) => {
		var missingTransaction = storage.grabAllAfterTimestamp(req.body.timestamp); 

		res.status(200).send({
			transactions: missingTransaction,
			timestamp: clock.getClock()
		}); 
	}
}

var onMessageReceive = (message, transaction) => {

	//Increment ack counter and save repsonse
	messages[message.timestamp].ackCounter++; 
	messages[message.timestamp].responses.push(transaction); 

	console.log("ACKS: " + messages[message.timestamp].ackCounter + "/" + Math.ceil(messages[message.timestamp].topologyCount / 2))

	//If we have only one or two nodes in the cluster it cannot have majority and will not deliver
	var tooSmallQuorum = messages[message.timestamp].topologyCount < 3; 
	var gotAtLeastOneAck = messages[message.timestamp].ackCounter > 0; 

	//Check for majority from qourum
	if(messages[message.timestamp].ackCounter > Math.ceil(messages[message.timestamp].topologyCount / 2) || (tooSmallQuorum && gotAtLeastOneAck)){
		
		if(!messages[message.timestamp].haveDelivered){
			messages[message.timestamp].haveDelivered = true;


			console.log(messages[message.timestamp])
			
			var highestTimestampTransaction = messages[message.timestamp].responses.sort((a, b) => {
				return parseInt(a.id.split("-")[1]) - parseInt(b.id.split("-")[1]); 
			})[0]; 

			console.log(messages[message.timestamp])
			console.log(highestTimestampTransaction)

			messages[message.timestamp].onDeliver(highestTimestampTransaction);
		}
	}
}

var askForLog = (message) => {
	try{
		request({
			method: 'POST',
			headers: {
				 "Content-Type": "application/json",
			},
			body: {
				sender: config.name,
				port: config.port,
				ip: config.ip,
				timestamp: clock.getClock(),
				origin: config.name,
				type: 'askForLog'
			},
			url: message.ip + ":" + message.port + "/message",
			json:true
		}, (err, httpResponse, body) => {

			//Just commit the missing transactions
			body.transactions.forEach((transaction) => {
				storage.commit(transaction); 
			});

			//Update local clock
			clock.merge(body.timestamp, clock.getClock()); 
		}); 
	}catch(e){	
		//We don't really care about this
		console.log(e)
	}
}

broadcast.bestEffortBroadcast = (message, type, onDeliver, useTimestamp) => {
	var topology = topologyManager.getTopology(); 

	console.log(clock.getClock()); 

	var timestamp =  !!(useTimestamp) ? useTimestamp : config.name + "-" + clock.getClock();
	var ack = {
		timestamp: timestamp,
		topologyCount: topologyManager.getTopology().length,
		ackCounter: 0,
		haveDelivered: false,
		responses: [],
		onDeliver: () => {
			onDeliver(arguments);
		}
	}

	messages[timestamp] = ack; 

	topology.forEach((node) => {
		try{
			request({
				method: 'POST',
				headers: {
					 "Content-Type": "application/json",
				},
				body: {
					sender: config.name,
					message: message,
					port: config.port,
					ip: config.ip,
					timestamp: ack.timestamp,
					origin: node.name,
					type: type
				},
				url: node.ip + ":" + node.port + "/message",
				json:true
			}, (err, httpResponse, body) => {
				//We don't care perticularly. Failure detection will get it
			}); 
		}catch(e){	
			//We don't really care about this
		}
	}); 

	return ack; 
}

/**
* This is only used for topology management
*/
broadcast.sendPing = function(addr){
	try{

		topologyManager.add(addr.ip, addr.port);

		request({
			method: 'POST',
			headers: {
				 "Content-Type": "application/json",
			},
			body: {
				type: "ping",
				sender: config.name,
				port: config.port,
				ip: config.ip,
				clock: clock.getClock(),
				topology: topologyManager.getTopology()
			},
			url: addr.ip + ":" + addr.port + "/message",
			json:true
		}, function(err, httpResponse, body){

			//Here we do failure detection
			if(err) {
				console.log("I should remove " + addr.ip + ":" + addr.port )
				failureDetector.suspect(addr.ip, addr.port)
				return;
			}

			//Here we get get the other nodes topology back, so we merge it with ours
			topologyManager.mergeTopology(body, config)
			
		}); 
	}catch(e){	
		//We don't really care about this
	}
}

broadcast.startListener = function(_config){

	//Save the config
	config = _config; 

	//Start up a server
	var server = app.listen(config.port, function () {
	   var host = server.address().address
	   var port = server.address().port   
	   console.log("Node: [" + config.name + "] listening on http://%s:%s", host, port)
	}); 

	//Receive a message from the cluster
	app.post('/message', function (req, res) {

		var message = req.body; 
		//Upsert to topology
		topologyManager.add(message.ip, message.port)

		//Execute endpoint logic
		router[req.body.type](req, res); 
	});

	//Receive a message from the client
	app.post('/v1/query', function (req, res) {

		var message = req.body; 

		//Execute endpoint logic
		router['clientQuery'](req, res); 
	});


	//Send our tpology every 5 sek
	new CronJob('*/5 * * * * *', function() {

		//Broadcast topology
		topologyManager.getTopology().forEach((addr) => {
			broadcast.sendPing(addr); 
		}); 
	}).start(); 
}



module.exports = broadcast; 