var topologyManager = {}; 

var topology = {}; 

topologyManager.add = (ip, port) => {
	
	if(!topology[ip]){
		topology[ip] = {}
	}

	topology[ip][port] = 1
}

topologyManager.mergeTopology = (top, config) => {
	top.forEach((_addr) => {
		//We don't add our own to the topology
		if(_addr.ip != config.ip || _addr.port != config.port){
			topologyManager.add(_addr.ip, _addr.port);
		}
	});
}

topologyManager.getTopology = () => {
	var res = [];

	Object.keys(topology).forEach((ip) => {
		Object.keys(topology[ip]).forEach((port) => {
			res.push({
				ip: ip,
				port: port
			})
		})
	});

	return res; 
}

module.exports = topologyManager; 