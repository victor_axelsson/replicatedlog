var clock = {}; 

var c = 0; 

clock.getClock = () => {
	return c;
}

clock.tick = () => {
	c++; 
}

var getCount = (str) => {
	if(isNaN(str)){
		return parseInt(str.split("-")[1]);
	}else{
		return parseInt(str);
	}
}

clock.merge = (time1, time2) => {
	c = Math.max(getCount(time1), getCount(time2));
}

module.exports = clock; 