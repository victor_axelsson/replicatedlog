var failureDetector = {}

var suspected = {}

failureDetector.suspect = (ip, port) => {
	if(!suspected[ip]){
		suspected[ip] = {}
	}
	suspected[ip][port] = 1
}

failureDetector.isSuspected = (ip, port) => {
	return suspected[ip] && suspected[ip][port];
}

failureDetector.restore = (ip, port) => {
	if(Object.keys(suspected[ip]).length > 1){
		delete suspected[ip][port]
	}else{
		delete suspected[ip]
	}
}

failureDetector.getSuspected = () => {
	return suspected;
}


module.exports = failureDetector