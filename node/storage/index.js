var storage = {}
var config = null;
var lastId = 0;
var store = {}
var clock = require('../broadcast/clock')
var log = require('./log')

storage.setConfig = (_config) => {
	config = _config; 
}

storage.createTransaction = (query) => {

	var transaction = {}
	query = query.toLowerCase().trim();
	var pts = query.replace(/\(/g, '').split(")")

	transaction.traversal = pts[0].split(",")
	transaction.action = pts[1]

	for(var i = 2; i < pts.length; i++){
		var marker = pts[i].trim();

		if(marker.length <= 0){
			continue; 
		}

		var tag = marker.match(/\:([a-z0-9]+)/g)[0] 

		if(tag == ":values"){

			var firstChar = marker.replace(tag, "").trim()[0]

			if(firstChar == "{" || firstChar == "["){
				transaction.values = JSON.parse(marker.replace(tag, "").trim())

			}else{
				transaction.values = JSON.parse(marker.match(/\"([a-z0-9]+)\"/g)[0])
			}
		}
	}

	transaction.id = config.name + "-" + clock.getClock()
	
	return transaction; 
}


storage.commit = (transaction) => {

	var traverse = (_store, queue, lazy_create) => {
		var c = queue[0]

		if(!(c in _store)){

			if(!lazy_create){
				return null; 
			}

			_store[c] = {}
		}

		if(queue.length > 1){
			return traverse(_store[c], queue.slice(1), lazy_create)
		}else{
			return _store
		}
	}

	if(transaction.action !== ":get"){
		log.append(transaction)
	}

	if(transaction.action == ":set"){
		var cursor = traverse(store, transaction.traversal, true)
		cursor[transaction.traversal[transaction.traversal.length -1]] = transaction.values
	}else if(transaction.action == ":get"){
		var cursor = traverse(store, transaction.traversal, false)
		transaction.result = cursor
	}else if(transaction.action == ":delete"){
		var cursor = traverse(store, transaction.traversal, false)

		if(cursor != null){
			delete cursor[transaction.traversal[transaction.traversal.length -1]]
		}
	}

	return transaction; 
}

storage.getStore = () => {
	return store;
}

storage.grabAllAfterTimestamp = (timestamp) => {
	var ts = parseInt(timestamp); 

	return log.getLog().filter((transaction) => {
		var id = parseInt(transaction.id.split("-")[1])
		return id >= ts; 
	}); 
}

module.exports = storage;